<?php

namespace App\Http\Controllers;

use App\Compani;
use Illuminate\Http\Request;

class CompaniController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Compani  $compani
     * @return \Illuminate\Http\Response
     */
    public function show(Compani $compani)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Compani  $compani
     * @return \Illuminate\Http\Response
     */
    public function edit(Compani $compani)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Compani  $compani
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Compani $compani)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Compani  $compani
     * @return \Illuminate\Http\Response
     */
    public function destroy(Compani $compani)
    {
        //
    }
}
